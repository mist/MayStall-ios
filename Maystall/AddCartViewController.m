//
//  AddCartViewController.m
//  Maystall
//
//  Created by DAWEI FAN on 10/03/2014.
//  Copyright (c) 2014 huiztech. All rights reserved.
//

#import "AddCartViewController.h"
#import "MayColorValue.h"
#import "MayValue.h"
#import "AddCartCell.h"
@interface AddCartViewController ()

@end

@implementation AddCartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    label_Price=[[UILabel alloc]init];
    label_totalPrice=[[UILabel alloc]init];
    [self createNavigationBarItem];
    [self createView];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.tabBarController.navigationController.navigationBarHidden = NO;
    self.navigationItem.title =@"Cart";
    self. navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor];//更改导航栏标题颜色 为白色
}
- (void)createNavigationBarItem
{
    //左键
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = CGRectMake(0, 0, 60, 31);
    [leftButton setTitle:@"Back" forState:UIControlStateNormal];
    [leftButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13]];
    [leftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(backToPreviousView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setBackgroundImage:[UIImage imageNamed:@"btn_nav_back"] forState:UIControlStateNormal];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] init];
    [leftItem setCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    //右键
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(0, 0, 40, 31);
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(backToPreviousView) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setBackgroundImage:[UIImage imageNamed:@"btn_nav_home"] forState:UIControlStateNormal];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] init];
    [rightItem setCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = rightItem;
}
- (void)createView
{
//    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
//    topView.backgroundColor = TAB_COLOR_DARK;
    UIImageView *topView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    topView.image=[UIImage imageNamed:@"search_box_bottom@2x.png"];
    [self.view addSubview:topView];
    
    UILabel *totalPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 100, 20)];
    totalPriceLabel.backgroundColor = [UIColor clearColor];
    totalPriceLabel.text = @"Total price(RM):";
    totalPriceLabel.textColor = TAB_COLOR_LIGHT;
    totalPriceLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    [topView addSubview:totalPriceLabel];
    
    label_totalPrice.frame =CGRectMake(150, 10, 100, 20);
    label_totalPrice.textColor = [UIColor redColor];
    label_totalPrice.text = @"200.00";
    label_totalPrice.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    [topView addSubview:label_totalPrice];
    
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 30, 70, 20)];
    priceLabel.textColor = TAB_COLOR_LIGHT;
    priceLabel.text = @"price(RM):";
    priceLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    [topView addSubview:priceLabel];
    
    label_Price.frame =CGRectMake(120, 30, 100, 20);
    label_Price.textColor = [UIColor redColor];
    label_Price.text = @"200.00";
    label_Price.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    [topView addSubview:label_Price];
  /*
    UILabel *cashBackLabel = [[UILabel alloc]initWithFrame:CGRectMake(150, 30, 70, 20)];
    cashBackLabel.textColor = TAB_COLOR_LIGHT;
    cashBackLabel.text = @"cashback:";
    cashBackLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    [topView addSubview:cashBackLabel];
    
    
    cashBack = [[UILabel alloc] initWithFrame:CGRectMake(220, 30, 100, 20)];
    cashBack.textColor = [UIColor redColor];
    cashBack.text = @"￥2000.00";
    cashBack.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
    [topView addSubview:cashBack];
    
    UIButton *selectAll = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
    selectAll.backgroundColor = [UIColor yellowColor];
    [selectAll addTarget:self action:@selector(selectallMethod) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:selectAll];
    */
    mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, 320, VIEW_HEIGHT-99)];
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.dataSource = self;
    mainTableView.delegate = self;
    mainTableView.separatorColor= [UIColor clearColor];
    mainTableView.sectionHeaderHeight = 2.0;
    mainTableView.sectionFooterHeight = 2.0;
    
    [self.view addSubview:topView];
    [self.view addSubview:mainTableView];
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddCartCell *cell=(AddCartCell*)[tableView dequeueReusableCellWithIdentifier:@"AddCartCell"];
    static NSString *cellIdentifiter = @"Cellidentifiter";
    if(cell==nil)
    {
        cell= [[AddCartCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellIdentifiter];
        //cell_last.contentView.backgroundColor = [UIColor whiteColor];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddCartCell" owner:[AddCartCell class] options:nil];
        cell = (AddCartCell *)[nib objectAtIndex:0];
      //  cell.contentView.backgroundColor = [UIColor whiteColor];
       
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.size.text=@"L";
    cell.color.text=@"blue";
    cell.name.text=@"hahahahahahahahaha";
    cell.numbers.text=@"13";
    return cell;

}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    return [UIView new];
//}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return indexPath;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 130;
    
    
}
-(void)tableView:(UITableView*)tableView  willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
    
}
- (void)backToPreviousView
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
